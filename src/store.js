'use strict'

const StoreFactory = require('./store/factory')

const config = require('./config')

let store

module.exports = (logger) => {
  if (!store) {
    logger.info('Store initialization')
    store = StoreFactory(config)
  }

  return store
}
