'use strict'

const winston = require('winston')

winston.format.hostname = winston.format((info, opts) => {
  info.hostname = opts.hostname

  return info
})

module.exports = function (config) {
  winston.add(new winston.transports.Console({
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.printf(({ level, message, timestamp }) => {
        return `${timestamp} ${level} ${message}`
      })
    )
  }))

  winston.add(new winston.transports.File({
    filename: 'log/docker-app.json',
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.hostname({ hostname: process.env.HOSTNAME || 'unknown' }),
      winston.format.json()
    )
  }))

  winston.level = config.get('log.level')

  return winston
}
