'use strict'

const AccessLog = require('./middleware/access-log')
const BodyParser = require('koa-bodyparser')
const Koa = require('koa')
const Logger = require('./logger')
const ResponseTime = require('koa-response-time')
const Router = require('@koa/router')

const config = require('./config')

const app = new Koa()
const logger = new Logger(config)
const router = new Router()
const status = require('http-status')
const store = require('./store')

const pathPrefix = (path, request) => {
  const prefix = config.has('pathPrefix')
    ? config.get('pathPrefix') : request.get('X-Forwarded-Prefix')

  return [prefix, path].join('')
}

router.get('root.get', '/', ({ request, response }) => {
  response.body = {
    links: {
      exception: pathPrefix(router.route('exception.get').url(), request),
      health: pathPrefix(router.route('health.get').url(), request),
      hello: pathPrefix(router.route('hello.get').url(), request),
      redirect: pathPrefix(router.route('redirect.get').url(), request),
      self: pathPrefix(router.route('root.get').url(), request)
    }
  }
})

let healthStatus = status.OK

router.get('health.get', '/health', ({ response }) => {
  logger.debug('Health check status invoked')
  response.status = healthStatus
})

router.put('health.put', '/health', ({ request, response }) => {
  switch (request.body.status) {
    case true:
      healthStatus = status.OK
      response.status = status.OK
      break
    case false:
      healthStatus = status.SERVICE_UNAVAILABLE
      response.status = status.OK
      break
    default:
      response.status = status.BAD_REQUEST
  }
})

router.get('exception.get', '/exception', () => {
  throw new Error('Unhandled test exception')
})

router.put('kill.put', '/kill', () => {
  process.exit(1)
})

router.put('stop.put', '/stop', ({ response }) => {
  logger.debug('Forced server shutdown')
  response.status = status.OK
  server.close()
})

const helloMessage = config.get('hello.message')
router.get('hello.get', '/hello', ({ response }) => {
  response.body = helloMessage
  response.status = status.OK
})

router.get('redirect.get', '/redirect', ({ request, response }) => {
  response.status = status.FOUND
  response.set('Location', pathPrefix(router.route('hello.get').url(), request))
})

router.post('data.post', '/data', async ({ request, response }) => {
  try {
    const id = await store(logger).post(request.body)
    response.set('Location',
      pathPrefix(router.route('data.get').url({ id }), request)
    )
    response.status = status.CREATED
  } catch (error) {
    logger.error(error.message)
    response.status = status.INTERNAL_SERVER_ERROR
  }
})

router.get('data.get', '/data/:id', async ({ response, params }) => {
  try {
    const data = await store(logger).get(params.id)
    response.body = data
    response.status = data ? status.OK : status.NOT_FOUND
  } catch (error) {
    logger.error(error.message, { details: error })
    response.status = status.INTERNAL_SERVER_ERROR
  }
})

app.use(new AccessLog(console))
app.use(new ResponseTime())
app.use(new BodyParser())
app.use(router.routes())
app.use(router.allowedMethods())

app.on('error', (error) => {
  logger.error(error.stack)
})

const server = app.listen(8080, () => {
  logger.info('Listening on port 8080')
})

function cleanup (signal) {
  logger.info(`Closing on ${signal} ...`)
  server.close()
}

process.on('SIGINT', cleanup)
process.on('SIGTERM', cleanup)
