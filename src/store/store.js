'use strict'

const crypto = require('crypto')

class Store {
  constructor (driver) {
    this.driver = driver
  }

  _getId (data) {
    return crypto.createHash('sha1').update(data).digest('hex')
  }

  async get (id) {
    const data = await this.driver.get(id)

    return data ? JSON.parse(data) : undefined
  }

  async post (data) {
    const _data = JSON.stringify(data)
    const id = this._getId(_data)
    await this.driver.post(id, _data)

    return id
  }
}

module.exports = Store
