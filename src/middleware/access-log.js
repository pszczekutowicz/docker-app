'use strict'

const util = require('util')
const { DateTime } = require('luxon')

module.exports = function (logger) {
  if (!logger) {
    logger = console
  }

  const format = '%s - - [%s] "%s %s HTTP/%s" %d %s'

  return async function (context, next) {
    const date = DateTime.fromJSDate(new Date()).toFormat('d/LLL/yyyy:HH:mm:ss ZZ')

    await next()

    const length = context.length ? context.length.toString() : '-'
    logger.debug(util.format(format, context.ip, date, context.method, context.path, context.req.httpVersion, context.status, length))
  }
}
