'use strict'

const fs = require('fs').promises
const path = require('path')

class File {
  constructor (options = {}) {
    this.path = options.path || undefined
  }

  _getFilename (id) {
    return path.format({
      dir: this.path,
      name: id,
      ext: '.json'
    })
  }

  get (id) {
    return fs.readFile(this._getFilename(id))
  }

  post (id, data) {
    return fs.writeFile(this._getFilename(id), data)
  }
}

module.exports = File
