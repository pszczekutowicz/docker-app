'use strict'

class Memory {
  constructor (data = {}) {
    this.data = data
  }

  get (id) {
    return this.data[id] || undefined
  }

  post (id, data) {
    this.data[id] = data
  }
}

module.exports = Memory
