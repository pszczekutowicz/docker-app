'use strict'

const Client = require('consul')

class Consul {
  constructor (options = {}) {
    this.client = new Client({ ...options, promisify: true })
    this.prefix = options.prefix || undefined
  }

  _getKey (...args) {
    return [this.prefix, ...args].filter(value => !!value).join('/')
  }

  list () {
    return this.client.kv.keys(this._getKey())
  }

  async get (id) {
    const response = await this.client.kv.get(this._getKey(id))

    return response && 'Value' in response ? response.Value : undefined
  }

  post (id, data) {
    return this.client.kv.set(this._getKey(id), data)
  }

  delete (id) {
    return this.client.kv.delete(this._getKey(id))
  }
}

module.exports = Consul
