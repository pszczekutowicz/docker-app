'use strict'

const Consul = require('./consul')
const File = require('./file')
const Memory = require('./memory')
const Store = require('./store')

const createDriver = config => {
  const driver = config.get('store.driver')
  switch (driver) {
    case 'consul':
      return new Consul(config.get('store.consul'))
    case 'file':
      return new File(config.get('store.file'))
    case 'memory':
      return new Memory(config.get('store.memory'))
    default:
      throw new Error(`Driver ${driver} does not exist`)
  }
}

module.exports = (config) => new Store(createDriver(config))
